package сom.celt.ikea;

import android.test.suitebuilder.annotation.SmallTest;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static com.celt.ikea.Utils.checkAndConvertNullToEmpty;

@SmallTest
@RunWith(RobolectricTestRunner.class)
public class TestUtils {
    @Test
    public void testCheckAndConvertNullToEmpty()   {
        Assert.assertEquals(checkAndConvertNullToEmpty("123"),"123");
    }
    @Test
    public void testCheckAndConvertNullToEmpty_Null()   {
        Assert.assertEquals(checkAndConvertNullToEmpty(null),"");
    }
    @Test
    public void testCheckAndConvertNullToEmpty_Empty()   {
        Assert.assertEquals(checkAndConvertNullToEmpty(""),"");
    }
    @Test
    public void testCheckAndConvertNullToEmpty_StringNull()   {
        Assert.assertEquals(checkAndConvertNullToEmpty("null"),"");
    }
}
