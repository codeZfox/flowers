package com.celt.ikea;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.util.Linkify;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class Cont extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contacts);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }


        TextView textView = (TextView) findViewById(R.id.myText);
        textView.setTypeface(Typeface.createFromAsset(getAssets(), "9151.ttf"));

        String data = "" +
                "Мы всегда рады общению с клиентами:\n" +
                "\n" +
                "URL: Сайта пока у нас нет/ \n" +
                "Email: ideadboom@gmail.com \n" +
                "Телефон(MTS): +375298737294 \n" +
                "Телефон(Velcom): +375444828406 \n" +
                "Адрес: г.Минск, ул. Сурганова 37/2 \n" +
                "\n";
        textView.setTextSize(27);
        textView.setText(data);
        Linkify.addLinks(textView, Linkify.ALL);



    }
}
