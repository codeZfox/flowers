package com.celt.ikea;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.text.DateFormat;
import java.util.Date;
import java.util.Properties;

public class MyDialogFragment extends DialogFragment {
    private int pos = 0;
    private EditText editName;
    private EditText editPhone;
    private String strtext;
    private Session session = null;
    private ProgressDialog pdialog = null;
    private String email, subject, textMessage, password;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view2 = inflater.inflate(R.layout.fragment_order, null);
        builder.setView(view2);
        textMessage = "";
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        Spinner spinner = (Spinner) view2.findViewById(R.id.spinner);
        editName = (EditText) view2.findViewById(R.id.editName);
        editPhone = (EditText) view2.findViewById(R.id.editPhone);
        Bundle arguments;

        arguments = getArguments();
        strtext = arguments.getString("text");

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position1, long id) {
                pos = position1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        return builder.create();
    }

    @Override
    public void onStart() {
        super.onStart();
        AlertDialog alertDialog = (AlertDialog) getDialog();
        if (alertDialog != null) {
            Button positiveButton = alertDialog.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (editName.length() != 0 && editName.length() > 2 && editName.length() < 256) {

                        if (editPhone.length() == 7) {
                            email = "flowershopbntu@gmail.com";
                            password = "l7B8k4mV2";

                            textMessage += "Дата: " + DateFormat.getDateTimeInstance().format(new Date()) +
                                    "<br>Имя: " + editName.getText() + "<br>Номер телефона: +375 " +
                                    getResources().getStringArray(R.array.codes)[pos] +
                                    " " + editPhone.getText() + "<br><br>" + strtext;
                            subject = "IKEABY";


                            Properties props = new Properties();
                            props.put("mail.smtp.auth", "true");
                            props.put("mail.smtp.starttls.enable", "true");
                            props.put("mail.smtp.host", "smtp.gmail.com");
                            props.put("mail.smtp.port", "587");

                            session = Session.getDefaultInstance(props, new Authenticator() {
                                protected PasswordAuthentication getPasswordAuthentication() {
                                    return new PasswordAuthentication(email, password);
                                }
                            });

                            pdialog = ProgressDialog.show(getActivity(), "", "Отправка заказа...", true);

                            RetreiveFeedTask task = new RetreiveFeedTask();
                            task.execute();
                        } else {
                            Toast.makeText(getActivity(), "Введите номер телефона", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Введите имя", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }


    private class RetreiveFeedTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            try {
                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress(email));
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("fantombel@yandex.ru"));
                message.setSubject(subject);
                message.setContent(textMessage, "text/html; charset=utf-8");
                Transport.send(message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            pdialog.hide();
            Toast.makeText(getActivity(), "Заказ оформлен, ожидайте звонок", Toast.LENGTH_SHORT).show();
            dismiss();
        }
    }
}
