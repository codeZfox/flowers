package com.celt.ikea;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.celt.ikea.captcha.MathCaptcha;

public class CaptchaFragment extends DialogFragment {

    interface OnSuccessEventCallBack {
        void onSuccess();
    }

    private MathCaptcha captcha;
    private EditText editText;
    private OnSuccessEventCallBack callBack;

    public CaptchaFragment setCallBack(OnSuccessEventCallBack callBack) {
        this.callBack = callBack;
        return this;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.fragment_enter_captcha, null);
        builder.setView(view);
        editText = (EditText) view.findViewById(R.id.editCaptcha);
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        captcha = new MathCaptcha(600, 150, MathCaptcha.MathOptions.PLUS_MINUS);

        ((ImageView) view.findViewById(R.id.captcha)).setImageBitmap(captcha.getImage());

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        return builder.create();
    }

    @Override
    public void onStart() {
        super.onStart();
        AlertDialog alertDialog = (AlertDialog) getDialog();
        if (alertDialog != null) {
            Button positiveButton = alertDialog.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (captcha.checkAnswer(editText.getText().toString().trim())) {
                        dismiss();
                        callBack.onSuccess();
                    } else {
                        Toast.makeText(getActivity(), "Captcha is not match", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

}
