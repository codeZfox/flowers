package com.celt.ikea;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;

public class Flower implements Parcelable {
    private String name;

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Flower createFromParcel(Parcel in) {
            return new Flower(in);
        }

        public Flower[] newArray(int size) {
            return new Flower[size];
        }
    };

    private int price;
    private String img;
    private int count;
    private boolean check;
    private boolean order;

    public Flower(String name, int price, boolean check, String img, int count, boolean order) {
        this.name = name;
        this.price = price;
        this.check = check;
        this.img = img;
        this.count = count;
        this.order = order;
    }


    public Flower(Parcel parcel) {
        Bundle bundle = parcel.readBundle();
        this.name = bundle.getString("name");
        this.price = bundle.getInt("price");
        this.img = bundle.getString("img");
        this.order = true;
    }
    public Flower(String name,int price,String img,boolean check) {
        this.name = name;
        this.price =price;
        this.img = img;
        this.check = check;
    }

    public Flower() {

    }

    public void setName(String name) {
        this.name = name;
    }

    public static Creator getCREATOR() {
        return CREATOR;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public void setOrder(boolean order) {
        this.order = order;
    }

    public boolean isOrder() {
        return order;
    }

    public boolean isCheck() {
        return check;
    }

    public int getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }


    public String getImg() {
        return img;
    }


    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setCountPlus() {
        if (count < 100) count++;
    }

    public void setCountMin() {
        if (count > 1) count--;
    }

    public String getTextMail() {
        return " Цена: " + price + " р. - " + count + " шт. Название: " + name;
    }

    public void updateOrder() {
        order = !order;
    }

    JSONObject getJSONObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", name);
            jsonObject.put("price", price);
            jsonObject.put("img", img);
            jsonObject.put("count", count);
            jsonObject.put("order", order);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return jsonObject;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        Bundle bundle = new Bundle();

        bundle.putString("name", name);
        bundle.putInt("price", price);
        bundle.putString("img", img);

        parcel.writeBundle(bundle);
    }
}
