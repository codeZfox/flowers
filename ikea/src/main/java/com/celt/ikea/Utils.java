package com.celt.ikea;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Utils {
    public static final String URL = "http://dpflowersshop.ucoz.net/1.xls";
    public static final String BUYLIST = "buylist";
    public static final String DATA = "data";



    public static String checkAndConvertNullToEmpty(String text) {
        if (text == null || text.isEmpty() || text.equals("null"))
            return "";
        return text;
    }


    static File downloadDatabase(Context context) throws IOException {
        File file = File.createTempFile("db.db", ".xls", context.getExternalCacheDir());

        final java.net.URL myURL = new URL(URL);
        InputStream dataStream = myURL.openConnection().getInputStream();

        // Output stream to write file
        OutputStream output = new FileOutputStream(file);

        byte data[] = new byte[4096];

        int count;
        while ((count = dataStream.read(data)) != -1) {
            output.write(data, 0, count);
        }

        // flushing output
        output.flush();

        // closing streams
        output.close();
        dataStream.close();

        return file;
    }



    public static ArrayList<Flower> jsonToList(String json) {
        ArrayList<Flower> list = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = jsonObject.getJSONArray(  DATA);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                String name = obj.optString("name", "");
                int price = obj.optInt("price", 0);
                boolean check = obj.optBoolean("check", false);
                String img = obj.optString("img", "");
                int count = obj.optInt("count", 1);
                boolean order = obj.optBoolean("order", true);
                list.add(new Flower(name, price, check, img, count, order));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;

    }

    static void saveData(SharedPreferences sPref, String data, String name) {
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString(name, data);
        ed.apply();
    }

    static String listToJson(List<Flower> list) {
        JSONObject jsonObject = new JSONObject();
        try {
            JSONArray array = new JSONArray();
            for (Flower item : list) {
                array.put(item.getJSONObject());
            }
            jsonObject.put(DATA, array);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    static ArrayList<Flower> getBuyList(SharedPreferences sPref) {
        return jsonToList(sPref.getString(BUYLIST, ""));
    }


    static void onBuy(final Activity activity, final SharedPreferences sharedPreferences, final Flower flower) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setTitle("Введите количество");

        final EditText editCount = new EditText(activity);
        editCount.setInputType(InputType.TYPE_CLASS_NUMBER);
        alert.setView(editCount);

        alert.setPositiveButton("OK", null);
        alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        final AlertDialog alertDialog = alert.create();

        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editCount.getText().length() != 0 && Integer.parseInt(String.valueOf(editCount.getText())) > 0 && Integer.parseInt(String.valueOf(editCount.getText())) < 101) {
                    ArrayList<Flower> list = jsonToList(sharedPreferences.getString(BUYLIST, ""));
                    flower.setCount(Integer.parseInt(String.valueOf(editCount.getText())));
                    list.add(flower);
                    saveData(sharedPreferences, listToJson(list), BUYLIST);
                    alertDialog.dismiss();
                    if (FlowerActivity.class.getName().contains(activity.getLocalClassName()))
                        activity.onBackPressed();
                } else {
                    Toast.makeText(activity, "Введите количество товаров от 1 до 100", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
