package com.celt.ikea.data;

import com.celt.ikea.Flower;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class DataHelper {

    public static List<Flower> dataReaderGeneric(String excelSheet, File excelFile) {
        List<HashMap<String, String>> rawData = readTestDataList(excelSheet, excelFile);
        List<Flower> flowers = new ArrayList<>();
        int i = 1;
        for (HashMap<String, String> row : rawData) {
            i++;
            Flower flower = new Flower();
            // set test case name

            for (String key : row.keySet()) {
                String value = row.get(key);

                switch (key.toLowerCase()) {
                    case "name":
                        flower.setName(value);
                        break;
                    case "price":
                        flower.setPrice(Integer.valueOf(value));
                        break;
                    case "check":
                        flower.setCheck(!value.equals("0"));
                        break;
                    case "img":
                        flower.setImg(value);
                        break;

                    default:
                        // Unsupported field in data

                }
            }
            flowers.add(flower);

        }
        return flowers;


    }

    public static List<HashMap<String, String>> readTestDataList(String dataSheetName, File xlsx) {
        try {
            String e = "./" + xlsx.getPath();
            Workbook workbook = new  HSSFWorkbook(new FileInputStream(new File(e)));
            // XSSFWorkbook wb = new XSSFWorkbook
            // Workbook wb = WorkbookFactory.create(new File(e));
            Sheet suiteSheet = workbook.getSheet(dataSheetName);
            //XSSFSheet suiteSheet = wb.getSheet(dataSheetName);
            int rowSuiteAmount = suiteSheet.getPhysicalNumberOfRows();
            Row suiteRow = null;
            //  XSSFRow suiteRow = null;
            Cell suiteCell = null;
            // XSSFCell suiteCell = null;
            Cell keyCell = null;
            //  XSSFCell keyCell = null;
            Row firstRow = suiteSheet.getRow(0);
            //  XSSFRow firstRow = suiteSheet.getRow(0);
            ArrayList resultList = new ArrayList();

            for (int k = 1; k < rowSuiteAmount; ++k) {
                suiteRow = suiteSheet.getRow(k);
                int cellAmount = suiteRow.getPhysicalNumberOfCells();
                HashMap dataMap = new HashMap();

                for (int n = 0; n < cellAmount; ++n) {
                    suiteCell = suiteRow.getCell(n);
                    keyCell = firstRow.getCell(n);
                    if (suiteCell != null) {
                        suiteCell.setCellType(1);
                        if (!(keyCell == null)) {
                            String cellString = suiteCell.getStringCellValue().trim();
                            keyCell.setCellType(1);
                            String keyString = keyCell.getStringCellValue().trim();
                            dataMap.put(keyString, cellString);
                        }
                    }
                }

                resultList.add(dataMap);
            }

            return resultList;
        } catch (Exception e) {

            return null;
        }
    }

}
