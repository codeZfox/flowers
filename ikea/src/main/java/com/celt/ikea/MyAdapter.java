package com.celt.ikea;

import android.app.Activity;
import android.content.Context;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import static com.celt.ikea.R.layout.list_item;
import static com.celt.ikea.Utils.onBuy;

public class MyAdapter extends BaseAdapter {

    private Activity activity;
    private List<Flower> list;
    private LayoutInflater layoutInflater;

    private DisplayImageOptions options;
    public MyAdapter(Activity context, List<Flower> list) {
        this.activity = context;
        this.list = list;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_stub)
                .showImageForEmptyUri(R.drawable.ic_empty)
                .showImageOnFail(R.drawable.ic_error)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
//                    .displayer(new CircleBitmapDisplayer(Color.WHITE, 5))
                .build();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) view = layoutInflater.inflate(list_item, parent, false);
        final Flower flower = getProduct(position);

        TextView text_name = (TextView) view.findViewById(R.id.text_name);
        text_name.setText(flower.getName());
        TextView text_price = (TextView) view.findViewById(R.id.text_price);
        text_price.setText(flower.getPrice() + " р.");
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
        ImageLoader.getInstance().displayImage(flower.getImg(), imageView,  options);
        Button button = (Button) view.findViewById(R.id.button);
        if(!flower.isCheck()) {
            button.setEnabled(false);
        }else {
            button.setEnabled(true);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBuy(activity, PreferenceManager.getDefaultSharedPreferences(activity), flower);
                }
            });
        }

        return view;
    }

    private Flower getProduct (int position){
        return (Flower) getItem(position);
    }
}