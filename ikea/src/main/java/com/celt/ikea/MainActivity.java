package com.celt.ikea;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.celt.ikea.data.DataHelper;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeader;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import java.util.ArrayList;

import static com.celt.ikea.Utils.*;

public class MainActivity extends AppCompatActivity {

    private Drawer.Result drawerResult;
    private ListView listView;
    private TextView textPriceAll;
    private Button buttonOrder;
    private MyAdapter adapter;
    private MyAdapterBuy adapterBuy;
    private ArrayList<Flower> listFlowers = new ArrayList<>();
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        AccountHeader.Result header = initHeader();
        initNavigationDrawer(toolbar, header);

        listView = (ListView) findViewById(R.id.listView);

        textPriceAll = (TextView) findViewById(R.id.textPriceAll);
        textPriceAll.setVisibility(View.INVISIBLE);

        buttonOrder = (Button) findViewById(R.id.buttonOrder);
        buttonOrder.setVisibility(View.INVISIBLE);
        buttonOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOrderWithCaptcha();
            }
        });

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String savedText = sharedPreferences.getString(DATA, "");
        if (!savedText.isEmpty()) {
           /* Flower flower = new Flower("eaw",1,"e",true);
            listFlowers.add(flower);*/
            listFlowers = jsonToList(savedText);
            adapter = new MyAdapter(MainActivity.this, listFlowers);
            listView.setAdapter(adapter);
        }

        if (!isNetworkConnected()) {
            Toast.makeText(this, "Произошла сетевая ошибка. Проверьте, что подключение к интернету работает стабильно.", Toast.LENGTH_SHORT).show();
        } else {
            new dataCsv().execute();
        }


        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(this);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024);
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs();

        ImageLoader.getInstance().init(config.build());
    }

    private void initNavigationDrawer(final Toolbar toolbar, AccountHeader.Result header) {
        final Intent intent = new Intent(this, Cont.class);

        drawerResult = new Drawer()
                .withActivity(this)
                .withToolbar(toolbar)
                .withDisplayBelowToolbar(false)
                .withSliderBackgroundColor(Color.rgb(232, 234, 246))
                .withStatusBarColor(Color.rgb(142, 36, 170))
                .addDrawerItems(getDrawerItems()
                )
                .withAccountHeader(header)

                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l, IDrawerItem iDrawerItem) {

                        switch (iDrawerItem.getIdentifier()) {

                            case 0: {
                                drawerResult.setSelection(0, false);
                                listFlowers = jsonToList(sharedPreferences.getString(DATA, ""));
                                listView.setAdapter(adapter);

                                buttonOrder.setVisibility(View.INVISIBLE);
                                textPriceAll.setVisibility(View.INVISIBLE);
                                break;
                            }
                            case 1: {
                                listFlowers = getBuyList(sharedPreferences);
                                adapterBuy = new MyAdapterBuy(MainActivity.this, listFlowers);
                                listView.setAdapter(adapterBuy);

                                buttonOrder.setVisibility(View.VISIBLE);
                                textPriceAll.setVisibility(View.VISIBLE);
                                textPriceAll.setText(String.valueOf(price()) + " р.");
                                break;
                            }
                            case 2: {
                                startActivity(intent);
                                break;
                            }

                        }
                    }
                })
                .build();
    }

    private AccountHeader.Result initHeader() {

        return new AccountHeader()
                .withActivity(this)
                .withCompactStyle(true)
                .withHeaderBackground(R.drawable.header)
                .withTranslucentStatusBar(true)
                .withProfileImagesClickable(false)
                .withCurrentProfileHiddenInList(true)
                .withSelectionListEnabledForSingleProfile(false)
                .build();
    }

    private IDrawerItem[] getDrawerItems() {
        return new IDrawerItem[]{
                new PrimaryDrawerItem().withName("Каталог").withIdentifier(0).withTextColor(Color.rgb(0, 0, 0)),
                new PrimaryDrawerItem().withName("Корзина").withIdentifier(1).withTextColor(Color.rgb(0, 0, 0)),
                new PrimaryDrawerItem().withName("Контакты").withIdentifier(2).withTextColor(Color.rgb(0, 0, 0)),
                new PrimaryDrawerItem().withName("Обновить").withIdentifier(0).withTextColor(Color.rgb(0, 0, 0))};
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null;
    }

    public void clickItem(View view) {
        Intent intent = new Intent(getApplicationContext(), FlowerActivity.class);
        intent.putExtra("item", listFlowers.get(listView.getPositionForView(view)));
        if (listView.getAdapter().equals(adapter) && listFlowers.get(listView.getPositionForView(view)).isCheck())
            intent.putExtra("buy", true);
        else
            intent.putExtra("buy", false);
        startActivity(intent);
    }


    private int price() {
        int price = 0;
        for (Flower flower : listFlowers) {
            if (flower.isOrder())
                price += flower.getPrice() * flower.getCount();
        }
        return price;
    }

    public void onBuyOrder(final View view) {
        listFlowers.get(listView.getPositionForView(view)).updateOrder();
        textPriceAll.setText(String.valueOf(price()) + " р.");
        saveData(sharedPreferences, listToJson(listFlowers), BUYLIST);
    }

    public void onPlus(final View view) {
        listFlowers.get(listView.getPositionForView(view)).setCountPlus();
        updateAdapter();
        saveData(sharedPreferences, listToJson(listFlowers), BUYLIST);
    }

    public void onMin(final View view) {
        listFlowers.get(listView.getPositionForView(view)).setCountMin();
        updateAdapter();
        saveData(sharedPreferences, listToJson(listFlowers), BUYLIST);
    }

    public void onDel(final View view) {
        listFlowers.remove(listView.getPositionForView(view));
        updateAdapter();
        saveData(sharedPreferences, listToJson(listFlowers), BUYLIST);
    }

    private void updateAdapter() {
        adapterBuy.notifyDataSetChanged();
        textPriceAll.setText(String.valueOf(price()) + " р.");
    }

    String getOrder() {
        String text = "";
        for (Flower i : listFlowers) {
            if (i.isOrder()) text += i.getTextMail() + "<br>";
        }
        int totalPrice = 0;
        for (Flower i : listFlowers) {
            if (i.isOrder()) totalPrice += i.getPrice() * i.getCount();
        }
        text += "Итого: " + totalPrice + "р.";
        return text;
    }

    private void onOrderWithCaptcha() {
        new CaptchaFragment().setCallBack(new CaptchaFragment.OnSuccessEventCallBack() {
            @Override
            public void onSuccess() {
                onOrder();
            }
        }).show(getSupportFragmentManager(), "CaptchaFragment");
    }

    private void onOrder() {
        Bundle bundle = new Bundle();
        bundle.putString("text", getOrder());

        FragmentManager manager = getSupportFragmentManager();
        MyDialogFragment myDialogFragment = new MyDialogFragment();
        myDialogFragment.setArguments(bundle);
        myDialogFragment.show(manager, "dialog");
    }


    private class dataCsv extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            try {
                listFlowers = (ArrayList<Flower>) DataHelper.dataReaderGeneric("Flowers", downloadDatabase(MainActivity.this));
            } catch (final Exception ex) {
                Log.d("Some exception", ex.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            adapter = new MyAdapter(MainActivity.this, listFlowers);
            listView.setAdapter(adapter);
            Toast.makeText(MainActivity.this.getApplicationContext(), "Загружено", Toast.LENGTH_SHORT).show();
        }
    }
}
