package com.celt.ikea;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import static com.celt.ikea.R.layout.list_item_buy;

public class MyAdapterBuy extends BaseAdapter {

    private List<Flower> list;
    private LayoutInflater layoutInflater;

    private DisplayImageOptions options;
    public MyAdapterBuy(Context context, List<Flower> list) {
        this.list = list;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_stub)
                .showImageForEmptyUri(R.drawable.ic_empty)
                .showImageOnFail(R.drawable.ic_error)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
//                    .displayer(new CircleBitmapDisplayer(Color.WHITE, 5))
                .build();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) view = layoutInflater.inflate(list_item_buy, parent, false);
        Flower product = getProduct(position);

//        TextView text_text = (TextView) view.findViewById(R.id.text_text);
//        text_text.setText(product.getText());
        TextView text_name = (TextView) view.findViewById(R.id.text_name);
        text_name.setText(product.getName());
        TextView text_price = (TextView) view.findViewById(R.id.text_price);
        text_price.setText(product.getPrice() + " р.");
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
        TextView text_count = (TextView) view.findViewById(R.id.textCount);
        text_count.setText(String.valueOf(product.getCount()));
        CheckBox checkbox = (CheckBox)view.findViewById(R.id.checkBox);
        if (product.isOrder()) checkbox.setChecked(true);
        else checkbox.setChecked(false);

        ImageLoader.getInstance().displayImage(product.getImg(), imageView,  options);
//            inflater = LayoutInflater.from(MainActivity.this);

        return view;
    }

    private Flower getProduct (int position){
        return (Flower) getItem(position);
    }
}